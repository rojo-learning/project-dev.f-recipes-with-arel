class RecipesController < ApplicationController
  # 'GET /recipes'
  # 'GET /recipes/only_with/1,2'
  def index
    if params[:ingredients]
      @recipes = recipes_only_with params[:ingredients]
    else
      @recipes = Recipe.all
    end

    render json: @recipes
  end

  private
  def recipes_only_with(search_ingredients)
    # Turn the ingredients ids string intro integers.
    search_ingredients = search_ingredients.split(',').map(&:to_i)

    # Get the tables representation from the models and connect to them.
    ingredients = Ingredient.arel_table; Ingredient.connection
    portions    = Portion.arel_table;    Portion.connection
    recipes     = Recipe.arel_table;     Recipe.connection

    # Build the select_manager to get the ids of recipes with ingredients that
    # are NOT wanted.
    unrequired = ingredients.project(recipes[:id])
      .where(ingredients[:id].not_in search_ingredients)
    unrequired.join(portions).on(ingredients[:id].eq portions[:ingredient_id])
    unrequired.join(recipes).on(portions[:recipe_id].eq recipes[:id]).distinct

    # Get the recipes that don't appear in the group on non-wanted recipes
    Recipe.where(Recipe[:id].not_in unrequired)
  end

  def recipe_params
    params.require(:recipe).permit(:ingredients)
  end
end

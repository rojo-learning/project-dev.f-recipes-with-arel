class Recipe < ActiveRecord::Base
  has_many :portions
  has_many :ingredients, through: :portions
  include ArelHelpers::ArelTable
end

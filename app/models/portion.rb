class Portion < ActiveRecord::Base
  belongs_to :recipe
  belongs_to :ingredient
  include ArelHelpers::ArelTable
end

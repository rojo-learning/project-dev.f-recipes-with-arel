class Ingredient < ActiveRecord::Base
  has_many :portions
  has_many :recipes, through: :portions
  include ArelHelpers::ArelTable
end

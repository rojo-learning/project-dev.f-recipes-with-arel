# Dev.f - Recipes with Arel
Small app that shows the use of Arel models to build complex queries that can be later used with ActiveRecord calls.

## Setup
Get the a copy of the repository:

`git clone https://akaiiro@bitbucket.org/akaiiro/dev.f-recipes-with-arel.git Recipes`


Set the required gems:

`cd Recipes`

`bundle`

Populate the database

`rake db:migrate`

`rake db:seed`

Start the rails server:

`rails s`

## Usage
Point your browser to [http://0.0.0.0:3000](http://0.0.0.0:3000)

The following endpoints are available and return only JSON data:

`GET /recipes`

`GET /recipes/only_with/:ingredients # /recipes/only_with/1,2`

# Buscando recetas
Se necesita encontrar las recetas que una persona puede preparar con ciertos
ingredientes (cosas que tiene en ese momento en su despensa y/o refrigerador).
Recetas que contengan ingredientes extras no deben ser retornadas ya que la
persona no cuenta con ellos.

## Datos
 - Ingredientes de la busqueda principal (params[:ingredients]):
   search_ingredients
 - Ingredientes no incluidos en la busqueda.
 - Recetas que contienen los ingredientes no incluidos en a busqueda:
   unrequired.

## Paso 1: Obtener los ingredientes que el usuario desea.
Requerirlos de la variable `paramas`.

```ruby
search_ingredients = params[:ingredients].split(',').map(&:to_i)
```

## Paso 2: Obtener recetas con los ingredientes que el usuario no tiene.
Obtener representaciones en Arel de los modelos Ingredient, Portion y Recipe.
Con esas representaciones construir un manejador de selección que represente la
consulta a los modelos.


```ruby
ingredients = Ingredient.arel_table; Ingredient.connection
portions    = Portion.arel_table;    Portion.connection
recipes     = Recipe.arel_table;     Recipe.connection

unrequired = ingredients.project(recipes[:id])
      .where(ingredients[:id].not_in search_ingredients)
unrequired.join(portions).on(ingredients[:id].eq portions[:ingredient_id])
unrequired.join(recipes).on(portions[:recipe_id].eq recipes[:id]).distinct
```


## Paso 3: Obtener recetas que contienen sólo ingredientes que el usuario tiene.
Pedir al modelo Recipe que devuelva recetas que no se encuentran dentro del
conjunto unrequired.

```ruby
Recipe.where(Recipe[:id].not_in unrequired)
```

# This file should contain all the record creation needed to seed the database
# with its default values. The data can then be loaded with the rake db:seed (or
# created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

recipes = [
  { name: 'Huevos con jamon'             },
  { name: 'Licuado de platano con fresa' },
  { name: 'Hot-cakes con tocino'         },
  { name: 'Hot-cakes con platano y miel' },
  { name: 'Fresas y platanos con miel'   }
]

ingredients = [
  { name: 'Huevos'          },
  { name: 'Jamon'           },
  { name: 'Tocino'          },
  { name: 'Harina de trigo' },
  { name: 'Leche'           },
  { name: 'Platano'         },
  { name: 'Fresas'          },
  { name: 'Vainilla'        },
  { name: 'Canela'          },
  { name: 'Mantequilla'     },
  { name: 'Miel de maple'   },
  { name: 'Miel de abeja'   }
]

portions = [
  { recipe_id: 1, ingredient_id: 1  },
  { recipe_id: 1, ingredient_id: 2  },
  { recipe_id: 2, ingredient_id: 5  },
  { recipe_id: 2, ingredient_id: 6  },
  { recipe_id: 2, ingredient_id: 7  },
  { recipe_id: 3, ingredient_id: 4  },
  { recipe_id: 3, ingredient_id: 5  },
  { recipe_id: 3, ingredient_id: 1  },
  { recipe_id: 3, ingredient_id: 10 },
  { recipe_id: 3, ingredient_id: 3  },
  { recipe_id: 3, ingredient_id: 11 },
  { recipe_id: 4, ingredient_id: 4  },
  { recipe_id: 4, ingredient_id: 5  },
  { recipe_id: 4, ingredient_id: 1  },
  { recipe_id: 4, ingredient_id: 10 },
  { recipe_id: 4, ingredient_id: 6  },
  { recipe_id: 4, ingredient_id: 12 },
  { recipe_id: 5, ingredient_id: 5  },
  { recipe_id: 5, ingredient_id: 6  },
  { recipe_id: 5, ingredient_id: 12 }
]

recipes.each     { |recipe|     Recipe.create recipe         }
ingredients.each { |ingredient| Ingredient.create ingredient }
portions.each    { |portion|    Portion.create portion       }

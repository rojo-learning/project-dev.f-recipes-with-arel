Rails.application.routes.draw do
  root 'recipes#index'

  get '/recipes', to: 'recipes#index'
  get '/recipes/only_with_ingredients/:ingredients', to: 'recipes#index'
end
